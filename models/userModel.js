var mongoose = require('mongoose');
var bcrypt = require('bcrypt');
var Schema = mongoose.Schema;

var user_exp = new Schema({
	fname: {type: String},
	lname: {type: String},
    email: {type: String,unique:true,required:true},
    profileImgPath: {type: String},
    profileImgName: {type: String},
    password: {type:String,required:true}
});


user_exp.pre('save',function(next){
    const user=this;
    const SALT_WORK_FACTOR=10;

    bcrypt.genSalt(SALT_WORK_FACTOR,function(err,salt){
        if(err)
            return next(err);
        bcrypt.hash(user.password,salt,function(err,hash){
            if(err)
                return next(err);
            user.password=hash;
            next();
        });
    });
});

user_exp.methods.comparePassword= function(candidatePassword,cb){
    bcrypt.compare(candidatePassword,this.password,function(err,isMatch){
        if(err)
            return cb(err);
        cb(null,isMatch);
    });
};  

module.exports = mongoose.model('user_exp', user_exp,'user_exp');