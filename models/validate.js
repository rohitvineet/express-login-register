

var expressJoi = require('express-joi');
var Joi = expressJoi.Joi;

module.exports.validate = {
    fname: expressJoi.Joi.types.String().min(1).max(25),
    lname: expressJoi.Joi.types.String().min(1).max(25),
    email: expressJoi.Joi.types.String(),
    password: expressJoi.Joi.types.String().alphanum().min(1).max(25)
};