'use strict';

const local = "mongodb://localhost/twitter";

const mongodbURI = {
    local: local
};

module.exports = {
    mongodbURI: mongodbURI
};