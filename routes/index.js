var express = require('express');
var async = require('async');
var multer = require('multer');
var expressJoi = require('express-joi');
var UserModel = require('../models/userModel');
var valid_schema = require('../models/validate');
var router = express.Router();
var upload = multer({dest: '/home/vineet/express-login-register/upload/'});
/* GET home page. */
router.get('/', function(req, res) {
  res.render('index');
});

router.post('/upload', expressJoi.joiValidate(valid_schema.validate),upload.single('file'), function(req, res, next) {
		var ext = req.file.mimetype.split("/");
		var data={
		fname:req.body.fname,
		lname:req.body.lname,
		email:req.body.email,
		profileImgPath:req.file.path+'.'+ext[1],
		profileImgName:req.file.filename+'.'+ext[1],
		password:req.body.password
	};
var user = new UserModel(data);
		user.save(user,(err)=>{
			if(err)
			{
				res.send("Data invalid!!Please try again");		
			}
			else
			{
				res.send("User created!!Please login again");
			}
		});
});


router.post('/login', function(request,response){
		async.waterfall([
								function(callback){
									UserModel.findOne({email:request.body.email},function(err,user){
									if(err)
										throw err;
									if(!user)
										callback(null,"UNF");
									else
										callback(null,user);
								});
								},
								function(arg,callback){
									if(arg==="UNF")
										callback(null,"UNF");
									else
										arg.comparePassword(request.body.password,function(err,isMatch){
											if(err)
												throw err;
											else if(isMatch)
											{
												callback(null,true);
											}
											else
												callback(null,false);
									});
								}
								],
								function(err,res){
									if(res=="UNF")
										response.send({message:"User not found!!"});
									else if(res==false)
										response.send({message:"Login Failed!!"});
									else 
										response.send({statusCode:200,message:"Login Successfull!!",data:res});
									}
								);
						});

module.exports = router;
